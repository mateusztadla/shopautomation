import Base.TestBase;
import Models.MenuItem;
import Pages.Manu.ManuPage;
import Pages.Manu.SubmenuPage;
import Pages.MyAccount.RegistrationPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

//rejestracja użytkownika
public class RegistrationTest extends TestBase {
    ManuPage manuPage;
    RegistrationPage registrationPage;

    @BeforeMethod
    public void baseChekout(){
        manuPage = new ManuPage(driver);
        registrationPage = new RegistrationPage(driver);
    }

    @Test
    public void register(){
        manuPage.openMenu(MenuItem.MYACCOUNT);
        registrationPage.login("qweasd@wp.pl", "qweaszxc!231e1dc1");
    }
}
