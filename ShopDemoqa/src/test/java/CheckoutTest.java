import Base.TestBase;
import Models.MenuItem;
import Pages.Basket.BasketPage;
import Pages.Basket.BasketPopupPage;
import Pages.Basket.ListOfProductsPage;
import Pages.Checkout.CheckoutConfimationPage;
import Pages.Checkout.CheckoutPage;
import Pages.Manu.ManuPage;
import Pages.Manu.SubmenuPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CheckoutTest extends TestBase {
    ManuPage manuPage;
    SubmenuPage submenuPage;
    ListOfProductsPage listOfProductsPage;
    BasketPopupPage basketPopupPage;
    BasketPage basketPage;
    CheckoutPage checkoutPage;
    CheckoutConfimationPage checkoutConfimationPage;

    @BeforeMethod
    public void baseCheckout(){
        manuPage = new ManuPage(driver);
        submenuPage = new SubmenuPage(driver);
        listOfProductsPage = new ListOfProductsPage(driver);
        basketPopupPage = new BasketPopupPage(driver);
        basketPage = new BasketPage(driver);
        checkoutPage = new CheckoutPage(driver);
        checkoutConfimationPage = new CheckoutConfimationPage(driver);
    }

    @Test
    public void Checkout(){
        manuPage.openMenu(MenuItem.WOMAN);
        submenuPage.selectSubmenuItem("Jackets");
        listOfProductsPage.addProductToBasket(1);
    }

}
