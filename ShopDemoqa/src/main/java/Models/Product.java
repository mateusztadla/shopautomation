package Models;

import org.testng.Assert;

import java.math.BigDecimal;

public class Product {
    String name;
    BigDecimal price;
    int quantity;

    public Product(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
        quantity =1;
    }
    public Product(String name, BigDecimal price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public BigDecimal getTotalCost(){
        return price.multiply(new BigDecimal(quantity));
    }
    public void addOneProduct(){
        quantity++;
    }
    public void compare(Product productFromPopup){
        // bo czasami ucina nazwy w popup
        // Assert.assertEquals(productFromPopup.getName(), name);
        Assert.assertEquals(productFromPopup.getPrice(), price);
        Assert.assertEquals(productFromPopup.getQuantity(), quantity);
    }
}
