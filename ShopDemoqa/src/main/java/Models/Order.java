package Models;

import org.testng.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<Product> products = new ArrayList<Product>();

    public List<Product> getProducts() {
        return products;
    }

    public void addNewProduct(String name, BigDecimal price) {
        for (Product product : products) {
            if (product.getName().equals(name)) {
                product.addOneProduct();
                return;
            }
        }
        products.add(new Product(name, price));
    }

    public BigDecimal getTotalCost() {
        BigDecimal totalCost = new BigDecimal(0);
        for (Product product : products) {
            totalCost.add(product.getTotalCost());
        }
        return totalCost;
    }

    public int getOrderQuantity() {
        int quantity = 0;
        for (Product product : products) {
            quantity += product.getQuantity();
        }
        return quantity;
    }

    public void compare(Order orderFromPopup) {
        List<Product> productsFromPopup = orderFromPopup.getProducts();
        Assert.assertEquals(productsFromPopup.size(), products.size());

        for (int i = 0; i < getProducts().size(); i++) {
            productsFromPopup.get(i).compare(products.get(i));
        }
    }
}
