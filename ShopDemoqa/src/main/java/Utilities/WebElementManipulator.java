package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class WebElementManipulator {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions actions;

    public WebElementManipulator(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
        actions = new Actions(driver);
    }

    public void elementIsVisible(WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void elementDisapeard(WebElement element){
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public WebElement getRandomElement(List<WebElement> elementList) {
        Random random = new Random();
        int randomNumber = random.nextInt((elementList.size() - 1));
        return elementList.get(randomNumber);
    }

    public Select getSelect(By by) {
        return new Select(driver.findElement(by));
    }
}
