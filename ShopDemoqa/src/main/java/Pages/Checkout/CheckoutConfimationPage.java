package Pages.Checkout;

import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CheckoutConfimationPage extends WebElementManipulator {
    public CheckoutConfimationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
