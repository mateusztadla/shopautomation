package Pages.Basket;

import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasketPopupPage extends WebElementManipulator {
    public BasketPopupPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
