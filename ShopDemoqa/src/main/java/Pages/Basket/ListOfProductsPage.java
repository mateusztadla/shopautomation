package Pages.Basket;

import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ListOfProductsPage extends WebElementManipulator {

    @FindBys(@FindBy(css = ".noo-product-item"))
    private List<WebElement> products;

    @FindBy(css = "ins .woocommerce-Price-amount")
    private WebElement priceAfterDiscount;

    @FindBy(css = ".add_to_cart_button")
    private WebElement addToCartButton;

    public ListOfProductsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void addProductToBasket(int i) {
        elementIsVisible(priceAfterDiscount);
    }
}
