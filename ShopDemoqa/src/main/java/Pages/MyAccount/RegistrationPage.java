package Pages.MyAccount;

import Utilities.WebElementManipulator;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.security.Key;

public class RegistrationPage extends WebElementManipulator {

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(id = "reg_password")
    private WebElement password;

    @FindBy(name = "register")
    private WebElement registerButton;


    @FindBy(className = "strong")
    private WebElement strongPassword;

    public RegistrationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public void login(String userEmail, String userPassword){
        email.sendKeys(userEmail);
        password.sendKeys(userPassword);
        hackButton();
        elementIsVisible(strongPassword);
        registerButton.click();
    }
    public void hackButton(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).keyUp(Keys.CONTROL).perform();
    }
}
