package Pages.Manu;

import Models.MenuItem;
import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManuPage extends WebElementManipulator {

    @FindBy(linkText = "MAN")
    private WebElement man;

    @FindBy(linkText = "WOMAN")
    private WebElement woman;


    @FindBy(linkText = "HOME")
    private WebElement home;


    @FindBy(id = "menu-item-2021")
    private WebElement myAccount;


    @FindBy(linkText = "SALE")
    private WebElement sale;


    public ManuPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void openMenu(MenuItem item) {
        switch (item) {
            case MAN:
                actions.moveToElement(man).perform();
                break;
            case WOMAN:
                actions.moveToElement(woman).perform();
                break;
            case MYACCOUNT:
                myAccount.click();
                break;
            case HOME:
                home.click();
                break;
            case SALE:
                sale.click();
                break;
            default:
                myAccount.click();
                break;
        }
    }
}
