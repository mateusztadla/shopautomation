package Pages.Manu;

import Utilities.WebElementManipulator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SubmenuPage extends WebElementManipulator {
    ////Xpath: nav[@class='noo-main-menu'] //li[@id='menu-item-2213'] /ul[@class ='sub-menu']
    @FindBy(css = "nav[class='noo-main-menu'] li[id='menu-item-2213'] >ul[class ='sub-menu']")
    private WebElement womanSubmenu;
    public SubmenuPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void selectSubmenuItem(String itemName){
        elementIsVisible(womanSubmenu);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(itemName)));
        element.click();
    }
}
